﻿using UnityEngine;
using System.Collections;

public class GameGUI : MonoBehaviour {
	
	public Texture _mainMenuButtonTexture;
	
	void OnGUI() {

		Rect buttonRect = new Rect(16.0f, 16.0f, _mainMenuButtonTexture.width, _mainMenuButtonTexture.height);
		if (GUI.Button(buttonRect, _mainMenuButtonTexture, GUIStyle.none)) {
			Application.LoadLevel(0);		
		}
	}
}
