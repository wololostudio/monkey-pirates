﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(SpriteRenderer))]

public class Fade : MonoBehaviour {

	public float _smooth = 6.0f;

	private Color _destinationColor;
	private SpriteRenderer _spriteRenderer;

	void Start() {
		_spriteRenderer = GetComponent<SpriteRenderer>();
	}

	void Update() {
		_spriteRenderer.color = Color.Lerp(_spriteRenderer.color, _destinationColor, Time.deltaTime * _smooth);
		Vector4 diffColor = (Vector4)_spriteRenderer.color - (Vector4)_destinationColor;
		if (diffColor.sqrMagnitude < 0.0001f) {
			enabled = false;
		}
	}

	public void FadeToColor(Color color) {
		_destinationColor = color;
		enabled = true;
	}
}
