﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor (typeof(Snap))]

public class Snap_Editor : Editor {

	void OnSceneGUI() {
		if (target != null) {
			((Snap)target).SnapPosition();
		}
	}
}
