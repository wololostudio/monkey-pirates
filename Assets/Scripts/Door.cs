﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Animator))]
[RequireComponent (typeof(BoxCollider))]

public class Door : MonoBehaviour {

	public PressurePlate _pressurePlate;
	public GridConnection _gridConnection;
	public Fade[] _fades;

	private Animator _animator;
	private BoxCollider _boxCollider;

	void Start () {

		_boxCollider = GetComponent<BoxCollider>();
		_animator = GetComponent<Animator>();
		_pressurePlate.OnActivatedValueChanged += OnActivatedValueChanged;
		if (_gridConnection) {
			_gridConnection._blocked = true;
		}
		for (int i = 0; i < _fades.Length; i++) {
			_fades[i].FadeToColor(Color.black);
		}
	}

	void OnActivatedValueChanged() {

		if (_pressurePlate.Activated) {
			SoundManager.Instance.PlayDoorOpenSound();
		}
		else {
			SoundManager.Instance.PlayDoorCloseSound();
		}

		_animator.SetBool("Opened", _pressurePlate.Activated);
		if (_gridConnection) {
			_gridConnection._blocked = !_pressurePlate.Activated;
		}
		_boxCollider.enabled = !_pressurePlate.Activated;

		Color color = _pressurePlate.Activated ? Color.white : Color.black;
		for (int i = 0; i < _fades.Length; i++) {
			_fades[i].FadeToColor(color);
		}
	}

}
