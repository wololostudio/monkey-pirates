﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {
	
	public Vector3 _axis = new Vector3(0, 0, 1);
	public float _speed = 1;

	private Transform _transform;
	
	void Start () {
		
		_transform = transform;
	}
	
	void Update () {
		
		_transform.Rotate(_axis, Time.deltaTime * _speed);
	}
}
