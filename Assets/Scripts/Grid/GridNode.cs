﻿using UnityEngine;
using System.Collections;
using System;

[ExecuteInEditMode]
[RequireComponent (typeof(BoxCollider))]

public class GridNode : MonoBehaviour {

	#region Unity params

	public Fade _destinationCircleFade;

	#endregion


	#region Public vars and properties

	public enum GridNoteType {
		Default,
		HisStart,
		HerStart,
		HisEnd,
		HerEnd,
	}

	public GridNoteType _type;
	
	public event Action OnOccupiedValueChanged;

	[HideInInspector]
	public int _x;
	[HideInInspector]
	public int _y;

	[HideInInspector]
	public Grid _grid;

	[HideInInspector]
	public GridConnection _connectionEast;
	[HideInInspector]
	public GridConnection _connectionWest;
	[HideInInspector]
	public GridConnection _connectionNorth;
	[HideInInspector]
	public GridConnection _connectionSouth;	

	public bool OccupiedByCharacter {
		get {
			return _occupiedByCharacter;
		}
		set {
			if (value == _occupiedByCharacter) {
				return;
			}
			_occupiedByCharacter = value;

			if (OnOccupiedValueChanged != null) {
				OnOccupiedValueChanged();
			}
		}
	}

	public GridConnection[] AllConnections {
		get {
			GridConnection[] gridConnections = new GridConnection[4];
			gridConnections[0] = _connectionEast;
			gridConnections[1] = _connectionWest;
			gridConnections[2] = _connectionNorth;
			gridConnections[3] = _connectionSouth;
			return gridConnections;
		}
	}

	public bool ShowDestinationCircle {
		
		set {
			_boxCollider.enabled = value;
			if (value) {
				_destinationCircleFade.FadeToColor(new Color(0.0f, 0.0f, 0.0f, 80.0f / 255.0f));
			}
			else {
				_destinationCircleFade.FadeToColor(Color.clear);
			}
		}
	}

	#endregion


	#region Private vars

	private BoxCollider _boxCollider;
	private bool _occupiedByCharacter;

	#endregion


	#region Unity callbacks

	void Start() {

		_boxCollider = GetComponent<BoxCollider>();
	}

	void OnDestroy() {
		
		if (_connectionEast) {
			DestroyImmediate(_connectionEast.gameObject);
		}
		
		if (_connectionWest) {
			DestroyImmediate(_connectionWest.gameObject);
		}
		
		if (_connectionNorth) {
			DestroyImmediate(_connectionNorth.gameObject);
		}
		
		if (_connectionSouth) {
			DestroyImmediate(_connectionSouth.gameObject);
		}
	}
	
	#endregion


	#region Actions

	public void CreateConnection(Grid.Direction direction) {

		if (GridConnectionForDirection(direction) != null) {
			return;
		}

		_grid.CreateConnection(this, direction);
	}

	private GridConnection GridConnectionForDirection(Grid.Direction direction) {

		switch (direction) {
		case Grid.Direction.North:
			return _connectionNorth;
		case Grid.Direction.South:
			return _connectionSouth;
		case Grid.Direction.West:
			return _connectionWest;
		case Grid.Direction.East:
			return _connectionEast;
		}

		return null;
	}

	public void SetConnectionForDirection(GridConnection gridConnection, Grid.Direction direction) {

		switch (direction) {

		case Grid.Direction.North:
			_connectionNorth = gridConnection;
			break;

		case Grid.Direction.South:
			_connectionSouth = gridConnection;
			break;

		case Grid.Direction.West:
			_connectionWest = gridConnection;
			break;

		case Grid.Direction.East:
			_connectionEast = gridConnection;
			break;
		}
	}

	public void RemoveConnection(GridConnection gridConnection) {

		if (gridConnection == _connectionEast) {
			_connectionEast = null;
		}
		else if (gridConnection == _connectionNorth) {
			_connectionNorth = null;
		}
		else if (gridConnection == _connectionSouth) {
			_connectionSouth = null;
		}
		else if (gridConnection == _connectionWest) {
			_connectionWest = null;
		}
	}

	#endregion
}
