﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Grid : Singleton<Grid> {

	public enum Direction {
		None 	= 0,
		North 	= 1,
		South 	= 1 << 1,
		East 	= 1 << 2,
		West	= 1 << 3,
	}

	public GameObject _gridNodePrefab;
	public GameObject _gridConnectionPrefab;

	public const int kGridSize = 30;
	public const int kGridOffset = 15;

	public GridNode[] GridNodes {
		get {
			return GetComponentsInChildren<GridNode>();
		}
	}

	public void Reset() {

		var children = new List<GameObject>();
		foreach (Transform child in transform) children.Add(child.gameObject);
		children.ForEach(child => DestroyImmediate(child));
	}

	public GridNode CreateNode(int x, int y) {

		if (GetNodeAtPos(x, y) != null) {
			return null;
		}

		GameObject obj = (GameObject)Instantiate(_gridNodePrefab, new Vector3(x - kGridOffset, transform.position.y, y - kGridOffset), Quaternion.identity);
		obj.name = "GridNode";
		obj.transform.parent = transform;
		GridNode gridNode = obj.GetComponent<GridNode>();
		gridNode._grid = this;
		gridNode._x = x;
		gridNode._y = y;

		return gridNode;
	}
	
	public GridConnection CreateConnection(GridNode gridNode, Direction direction) {

		Vector2 offset = OffsetFromDirection(direction);
		int connectedX = gridNode._x + (int)offset.x;
		int connectedY = gridNode._y + (int)offset.y;

		GridNode anotherNode = GetNodeAtPos(connectedX, connectedY);

		if (anotherNode == null) {
			anotherNode = CreateNode(connectedX, connectedY);
		}

		Vector3 newObjPos = new Vector3(gridNode._x + offset.x * 0.5f - kGridOffset, transform.position.y, gridNode._y + offset.y * 0.5f - kGridOffset);
		GameObject obj = (GameObject)Instantiate(_gridConnectionPrefab, newObjPos, Quaternion.identity);
		obj.name = "GridConnection";
		obj.transform.parent = transform;
		obj.transform.rotation = Quaternion.Euler(new Vector3(0.0f, RotationForDirection(direction), 0.0f));
		GridConnection gridConnection = obj.GetComponent<GridConnection>();


		gridNode.SetConnectionForDirection(gridConnection, direction);
		anotherNode.SetConnectionForDirection(gridConnection, OppositeDirectionForDirection(direction));

		gridConnection._node0 = gridNode;
		gridConnection._node1 = anotherNode;

		return gridConnection;
	}

	private GridNode GetNodeAtPos(int x, int y) {

		GridNode[] nodes = GridNodes;
		for (int i = 0; i < nodes.Length; i++) {

			if (nodes[i]._x == x && nodes[i]._y == y) {
				return nodes[i];
			}
		}
		return null;
	}

	public Vector2 OffsetFromDirection(Direction direction) {

		switch (direction) {
		case Grid.Direction.North:
			return new Vector2(0, 1);
		case Grid.Direction.South:
			return new Vector2(0, -1);
		case Grid.Direction.West:
			return new Vector2(-1, 0);
		case Grid.Direction.East:
			return new Vector2(1, 0);
		}

		return Vector2.zero;
	}

	public Direction OppositeDirectionForDirection(Direction direction) {

		switch (direction) {
		case Direction.North:
			return Direction.South;
		case Direction.South:
			return Direction.North;
		case Direction.West:
			return Direction.East;
		case Direction.East:
			return Direction.West;
		}

		return Direction.None;
	}

	public float RotationForDirection(Direction direction) {

		switch (direction) {
		case Direction.North:
			return 270.0f;
		case Direction.South:
			return 90.0f;
		case Direction.West:
			return 180.0f;
		case Direction.East:
			return 0.0f;
		}
		
		return 0.0f;
	}
}
