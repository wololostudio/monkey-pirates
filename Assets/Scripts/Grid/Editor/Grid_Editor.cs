﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor (typeof(Grid))]

public class Grid_Editor : Editor {
	
	public override void OnInspectorGUI() {

		if (target == null) {
			return;
		}

		Grid grid = (Grid)target;

		DrawDefaultInspector();

		GUILayout.Space(5.0f);

		if (GUILayout.Button("Create first node")) {
			grid.CreateNode(Grid.kGridOffset, Grid.kGridOffset);
		}

		GUILayout.Space(5.0f);

		if (GUILayout.Button("Reset")) {

			if (EditorUtility.DisplayDialog("Warning", "Are you sure you want to reset the grid?", "Reset", "Cancel")) {

				grid.Reset();
			}
		}
	}
}
