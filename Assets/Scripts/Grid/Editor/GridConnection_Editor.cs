﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof(GridConnection))]

public class GridConnection_Editor : Editor {

	public override void OnInspectorGUI() {
		
		if (target == null) {
			return;
		}
		
		GridConnection gridConnection = (GridConnection)target;
		
		DrawDefaultInspector();
		
		GUILayout.Space(5.0f);
		
		if (GUILayout.Button("Delete")) {
			DestroyImmediate(gridConnection.gameObject);
		}

	}
}
