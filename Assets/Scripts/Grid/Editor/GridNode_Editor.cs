﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor (typeof(GridNode))]

public class GridNode_Editor : Editor {

	public override void OnInspectorGUI() {
		
		if (target == null) {
			return;
		}

		GridNode gridNode = (GridNode)target;

		DrawDefaultInspector();

		GUILayout.Space(5.0f);

		if (GUILayout.Button("Create Connection North")) {

			gridNode.CreateConnection(Grid.Direction.North);
		}

		if (GUILayout.Button("Create Connection South")) {

			gridNode.CreateConnection(Grid.Direction.South);
		}

		if (GUILayout.Button("Create Connection East")) {

			gridNode.CreateConnection(Grid.Direction.East);
		}

		if (GUILayout.Button("Create Connection West")) {

			gridNode.CreateConnection(Grid.Direction.West);
		}

		GUILayout.Space(10.0f);

		if (GUILayout.Button("Delete")) {

			DestroyImmediate(gridNode.gameObject);
		}
	}

}
