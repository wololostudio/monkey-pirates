﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]

public class GridConnection : MonoBehaviour {

	[HideInInspector]
	public GridNode _node0;

	[HideInInspector]
	public GridNode _node1;

	public bool _blocked;

	public GridNode AnotherGridNode(GridNode gridNode) {

		if (gridNode != _node0) {
			return _node0;
		}
		else {
			return _node1;
		}
	}

	void OnDestroy() {

		if (_node0) {
			_node0.RemoveConnection(this);
		}

		if (_node1) {
			_node1.RemoveConnection(this);
		}
	}
}
