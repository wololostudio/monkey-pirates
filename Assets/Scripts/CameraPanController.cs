﻿using UnityEngine;
using System.Collections;

public class CameraPanController : MonoBehaviour {

	public Vector2 _bottomLeftBound = new Vector2(-1.0f, -5.0f);
	public Vector2 _topRightBound = new Vector2(1.0f, 5.0f);

	public Vector3 LookAtPos {
		set {

			Vector3 lookAtPos = value;
			lookAtPos.y = 0.0f;

			Ray ray = _camera.ScreenPointToRay(new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0.0f));
			float distance = 0.0f;
			if (_zeroPlane.Raycast(ray, out distance)) {
				PanByOffset((ray.GetPoint(distance) - lookAtPos));
			}
		}
	}

	public bool SlowMotion {
		set {

			if (_slowMotion == value) {
				return;
			}

			_slowMotion = value;

			_destinationPos = _cameraTransform.position;

			if (_slowMotion) {
				_elasticity = 2.0f;
				_speed = 10.0f;
			}
			else {
				_elasticity = 5.0f;
				_speed = 30.0f;
			}
		}
	}

	private bool _slowMotion = false;

	private float _elasticity = 5.0f;
	private float _speed = 30.0f;

	private Transform _cameraTransform;
	private Camera _camera;
	private Vector3 _destinationPos;
	private Plane _zeroPlane;
	private float _destinationFieldOfView;

	private Vector3 velocity;

	void Start () {

		_camera = Camera.main;
		_cameraTransform = _camera.transform;
		_destinationPos = _cameraTransform.position;

		_zeroPlane = new Plane(Vector3.up, Vector3.zero);

		_destinationFieldOfView = _camera.fieldOfView;
	}

	void Update () {
	
		Vector3 pos = _cameraTransform.position;

		velocity += (_destinationPos - pos) * Time.deltaTime * _elasticity;
		velocity *= 0.7f;
		_cameraTransform.position += velocity * Time.deltaTime * _speed;

		_camera.fieldOfView = Mathf.Lerp(_camera.fieldOfView, _destinationFieldOfView, Time.deltaTime * 5.0f);
		_destinationFieldOfView -= Input.GetAxis("Mouse ScrollWheel") * 10.0f;
		_destinationFieldOfView = Mathf.Clamp(_destinationFieldOfView, 30.0f, 80.0f);
	}
	
	public void PanByOffset(Vector3 offset) {

		_destinationPos.x -= offset.x;
		_destinationPos.z -= offset.z;

		_destinationPos.x = Mathf.Clamp(_destinationPos.x, _bottomLeftBound.x, _topRightBound.x);
		_destinationPos.z = Mathf.Clamp(_destinationPos.z, _bottomLeftBound.y, _topRightBound.y);
	}
}
