﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {

	public Transform _spriteTransform;
	public SpriteRenderer _spriteRenderer;
	public int _id;

	[HideInInspector]
	public bool _hovered;

	public bool Unlocked {
		get {
			return _unlocked;
		}
		set {

			_unlocked = value;
			_spriteRenderer.color = _unlocked ? Color.white : new Color(1.0f, 1.0f, 1.0f, 0.3f);
		}
	}

	private bool _unlocked;

	void Update() {

		if (_hovered && _unlocked) {
			_spriteTransform.localScale = Vector3.Lerp(_spriteTransform.localScale, new Vector3(1.2f, 1.2f, 1.2f), Time.deltaTime * 3.0f);
		}
		else {
			_spriteTransform.localScale = Vector3.Lerp(_spriteTransform.localScale, new Vector3(1.0f, 1.0f, 1.0f), Time.deltaTime * 3.0f);
		}
	}
}
