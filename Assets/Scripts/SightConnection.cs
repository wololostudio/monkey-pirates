﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(LineRenderer))]

public class SightConnection : MonoBehaviour {

	public Transform _hisCubeTransform;
	public Transform _herCubeTransform;
	public Color _himFullColor;
	public Color _herFullColor;
	public Color _noSightColor;

	private LineRenderer _lineRenderer;
	private Color _herColor;
	private Color _himColor;

	void Start () {

		_lineRenderer = GetComponent<LineRenderer>();
		_herColor = _herFullColor;
		_himColor = _himFullColor;
	}

	void Update () {

		Vector3 startPos = _hisCubeTransform.position;
		Vector3 endPos = _herCubeTransform.position;

		// Disconnected
		if (Physics.Raycast(new Ray(startPos, endPos - startPos), (endPos - startPos).magnitude)) {

			_herColor = Color.Lerp(_herColor, _noSightColor, Time.deltaTime * 3.0f);
			_himColor = Color.Lerp(_himColor, _noSightColor, Time.deltaTime * 3.0f);

			GameplayManager.Instance._her.SightHasBeenInterupted();
			GameplayManager.Instance._him.SightHasBeenInterupted();
		}
		// Connected
		else {

			_herColor = Color.Lerp(_herColor, _herFullColor, Time.deltaTime * 3.0f);
			_himColor = Color.Lerp(_himColor, _himFullColor, Time.deltaTime * 3.0f);

			GameplayManager.Instance._her.SightHasBeenConnected();
			GameplayManager.Instance._him.SightHasBeenConnected();
		}

		_lineRenderer.SetColors(_herColor, _himColor);

		_lineRenderer.SetPosition(1, startPos);
		_lineRenderer.SetPosition(0, endPos);
	}
}
