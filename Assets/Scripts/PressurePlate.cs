﻿using UnityEngine;
using System.Collections;
using System;

public class PressurePlate : MonoBehaviour {

	public GridNode _gridNode;
	public Fade[] _fades;

	public event Action OnActivatedValueChanged;

	public bool Activated {
		get {

			return _activated;
		}
		set {

			if (_activated == value) {
				return;
			}

			_activated = value;

			Color color = _activated ? Color.white : Color.black;
			for (int i = 0; i < _fades.Length; i++) {
				_fades[i].FadeToColor(color);
			}

			if (OnActivatedValueChanged != null) {
				OnActivatedValueChanged();
			}
		}
	}

	private bool _activated;
	
	void Start() {
		_gridNode.OnOccupiedValueChanged += OnOccupiedValueChanged;
		for (int i = 0; i < _fades.Length; i++) {
			_fades[i].FadeToColor(Color.black);
		}
	}

	void OnOccupiedValueChanged() {

		Activated = _gridNode.OccupiedByCharacter;
	}
}
