﻿using UnityEngine;
using System.Collections;

public class MainMenuManager : MonoBehaviour {

	public Button[] _levelButtons;

	private Camera _mainCamera;

	void Start () {
	
		_mainCamera = Camera.main;

		int numberOfUnlockedLevels = LevelModel.Instance.NumberOfUnlockedLevels;
		for (int i = 0; i < _levelButtons.Length; i++) {
			if (i < numberOfUnlockedLevels) {
				_levelButtons[i].Unlocked = true;
			}
			else {
				_levelButtons[i].Unlocked = false;
			}
		}
	}

	void Update() {

		for (int i = 0; i < _levelButtons.Length; i++) {
			_levelButtons[i]._hovered = false;
		}

		Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit)) {
			Button button = hit.collider.gameObject.GetComponent<Button>();
			if (button) {
				button._hovered = true;

				if (Input.GetMouseButtonUp(0)) {
					Application.LoadLevel(button._id + 1);
				}
			}
		}
	}

}
