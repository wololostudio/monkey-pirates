﻿using UnityEngine;
using System.Collections;
using System;

public class GestureManager : MonoBehaviour {

	public delegate void PanGestureHandler(Vector3 mousePos, Vector3 deltaPos, Vector3 sceneDeltaPos);
	public delegate void TapGestureHandler(Vector3 mousePos, Ray sceneRay);

	public event TapGestureHandler onTapGestureRecognized;
	public event PanGestureHandler onPanGestureRecognized;
	public event Action onPanGestureEnded;

	private Vector3 _touchDownPos;
	private Vector3 _prevPos;
	private Vector3 _prevScenePos;
	private bool _tapCancelled;

	private Plane _zeroPlane;
	private Camera _mainCamera;
	private Transform _mainCameraTransform;
	private Camera _gestureCamera;
	private Transform _gestureCameraTransform;

	void Start() {

		_zeroPlane = new Plane(Vector3.up, Vector3.zero);
		_mainCamera = Camera.main;
		_mainCameraTransform = _mainCamera.transform;
		_gestureCamera = new GameObject("GestureCamera").AddComponent<Camera>();
		_gestureCameraTransform = _gestureCamera.transform;
		_gestureCamera.enabled = false;
		_gestureCameraTransform.parent = transform;
	}
	
	void Update() {
	
		Vector3 mousePos = Input.mousePosition;

		if (Input.GetMouseButtonDown(0)) {

			_tapCancelled = false;
			_prevPos = mousePos;
			_prevScenePos = ScenePosFromMousePos(mousePos);
			_touchDownPos = _prevPos;
		}
		else if (Input.GetMouseButtonUp(0)) {

			// Tap
			if (!_tapCancelled) {
				if (onTapGestureRecognized != null) {
					onTapGestureRecognized(mousePos, _mainCamera.ScreenPointToRay(mousePos));
				}
			}
			// Panning ended
			else {
				if (onPanGestureRecognized != null) {
					onPanGestureRecognized(mousePos, mousePos - _prevPos, ScenePosFromMousePos(mousePos) - _prevScenePos);
				}
				if (onPanGestureEnded != null) {
					onPanGestureEnded();
				}
			}
		}

		if (Input.GetMouseButton(0)) {

			if ((mousePos - _touchDownPos).sqrMagnitude > 25.0f) {
				_tapCancelled = true;
			}

			if (_tapCancelled) {
				
				// Panning
				if (onPanGestureRecognized != null) {
					onPanGestureRecognized(mousePos, mousePos - _prevPos, ScenePosFromMousePos(mousePos) - _prevScenePos);
				}
			}
		}

		_prevPos = mousePos;
		_prevScenePos = ScenePosFromMousePos(mousePos);
	}

	public Vector3 ScenePosFromMousePos(Vector3 mousePos) {

		Vector3 cameraPos = _mainCameraTransform.position;
		_gestureCamera.fieldOfView = _mainCamera.fieldOfView;
		_gestureCameraTransform.position = new Vector3(0.0f, cameraPos.y, 0.0f);
		_gestureCameraTransform.rotation = _mainCameraTransform.rotation;

		Ray ray = _gestureCamera.ScreenPointToRay(mousePos);
		float distance = 0.0f;
		if (_zeroPlane.Raycast(ray, out distance)) {
			return ray.GetPoint(distance);
		}

		return Vector3.zero;
	}
}
