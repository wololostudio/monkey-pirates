﻿using UnityEngine;
using System.Collections;

public class LevelModel : MonoBehaviour {
	
	public static LevelModel Instance {
		get {
			if (instance == null) {
				instance = new GameObject("LevelModel").AddComponent<LevelModel>();
			}
			return instance;
		}
	}	
	
	public int NumberOfUnlockedLevels {
		get {
			return numberOfUnlockedLevels;
		}
	}
	
	private static LevelModel instance;
	private const string kNumberOfUnlockedLevelsKey = "LevelModel.NumberOfUnlockedLevelsKey";
	private int numberOfUnlockedLevels;
	
	public void UnlockLevel(int levelNum) {
		if (levelNum >= numberOfUnlockedLevels) {
			numberOfUnlockedLevels = levelNum;
			PlayerPrefs.SetInt(kNumberOfUnlockedLevelsKey, numberOfUnlockedLevels);
		}
	}
	
	void Awake () {
		
		numberOfUnlockedLevels = PlayerPrefs.GetInt(kNumberOfUnlockedLevelsKey, 1);
	}
}
