﻿using UnityEngine;
using System.Collections;
using System;

public class OutroManager : MonoBehaviour {

	public AnimationCurve _outroAnimationCurve;

	public event Action onOutroFinished;
	
	private Transform _mainCameraTransform;
	private Camera _mainCamera;
	private float _elapsedTime;
	private Vector3 _startPos;
	private Vector3 _endPos;
	private float _duration = 4.0f;

	void Start () {
		
		_mainCamera = Camera.main;
		_mainCameraTransform = _mainCamera.transform;
		_startPos = _mainCameraTransform.position;
		_endPos = _startPos + new Vector3(20.0f, 50.0f, 0.0f);
	}
	
	void Update () {
		
		_elapsedTime += Time.deltaTime;
		float t = _outroAnimationCurve.Evaluate(_elapsedTime / _duration);
		_mainCameraTransform.position = Vector3.Lerp(_startPos, _endPos, t);

		
		if (_elapsedTime >= _duration) {
			if (onOutroFinished != null) {
				onOutroFinished();
			}
			enabled = false;
		}
	}
}
