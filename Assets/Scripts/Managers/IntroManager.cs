﻿using UnityEngine;
using System.Collections;
using System;

public class IntroManager : MonoBehaviour {

	public AnimationCurve _introAnimationCurve;
	public float _duration = 1.0f;
	public Transform _startTransform;

	public event Action onIntroFinished;

	private Transform _mainCameraTransform;
	private Camera _mainCamera;
	private float _elapsedTime;
	private Vector3 _endPosition;
	private Quaternion _endRotation;
	
	void Start () {
	
		_mainCamera = Camera.main;
		_mainCameraTransform = _mainCamera.transform;
		_endPosition = _mainCameraTransform.position;
		_endRotation = _mainCameraTransform.rotation;


	}

	void Update () {
	
		_elapsedTime += Time.deltaTime;
		float t = _introAnimationCurve.Evaluate(_elapsedTime / _duration);
		_mainCameraTransform.position = Vector3.Lerp(_startTransform.position, _endPosition, t);
		_mainCameraTransform.rotation = Quaternion.Lerp(_startTransform.rotation, _endRotation, t);

		if (_elapsedTime >= _duration) {
			if (onIntroFinished != null) {
				onIntroFinished();
			}
			enabled = false;
		}
	}
}
