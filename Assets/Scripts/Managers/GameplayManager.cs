﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent (typeof(GestureManager))]
[RequireComponent (typeof(CameraPanController))]

public class GameplayManager : Singleton<GameplayManager> {
	
	public Character _her;
	public Character _him;
	public LayerMask _charactersLayerMask;
	public LayerMask _gridNodesLayerMask;

	public event Action onlevelFinished;

	private Character SelectedCharacter {

		get {
			if (_her.Selected) {
				return _her;
			}
			else if (_him.Selected) {
				return _him;
			}
			else {
				return null;
			}
		}
	}

	private bool _heIsAtTheEnd;
	private bool _sheIsAtTheEnd;

	private GestureManager _gestureManager;
	private CameraPanController _cameraPanController;
	
	void Start() {

		_gestureManager = GetComponent<GestureManager>();
		_cameraPanController = GetComponent<CameraPanController>();
		_gestureManager.onPanGestureRecognized += OnPanGestureRecognized;
		_gestureManager.onTapGestureRecognized += OnTapGestureRecognized;

		// Start pos
		GridNode[] gridNodes = Grid.Instance.GridNodes;
	
		for (int i = 0; i < gridNodes.Length; i++) {

			if (gridNodes[i]._type == GridNode.GridNoteType.HerStart) {
				_her.transform.position = gridNodes[i].transform.position;
				_her.GridNode = gridNodes[i];
			}
			else if (gridNodes[i]._type == GridNode.GridNoteType.HisStart) {
				_him.transform.position = gridNodes[i].transform.position;
				_him.GridNode = gridNodes[i];
			}
		}
	}
	
	public void OnPanGestureRecognized(Vector3 mousePos, Vector3 deltaPos, Vector3 sceneDeltaPos) {

		_cameraPanController.SlowMotion = false;
		_cameraPanController.PanByOffset(sceneDeltaPos);
	}

	public void OnTapGestureRecognized(Vector3 mousePos, Ray sceneRay) {

		RaycastHit raycastHit;

		// Selection
		Physics.Raycast(sceneRay, out raycastHit, 1000.0f, _charactersLayerMask);
		if (raycastHit.collider) {

			Character character = raycastHit.collider.gameObject.GetComponent<Character>();

			if (character.CanBeSelected) {

				if (character == _her) {
					_him.Selected = false;
				}
				else {
					_her.Selected = false;
				}

				character.Selected = true;
				_cameraPanController.LookAtPos = (_her.transform.position + _him.transform.position) * 0.5f;
				_cameraPanController.SlowMotion = true;
			}
		}
		else {

			// Movement
			Physics.Raycast(sceneRay, out raycastHit, 1000.0f, _gridNodesLayerMask);
			if (raycastHit.collider) {

				if (SelectedCharacter) {

					GridNode gridNode = raycastHit.collider.GetComponent<GridNode>();
					_cameraPanController.LookAtPos = Vector3.Lerp(AnotherCharacter(SelectedCharacter).transform.position, gridNode.transform.position, 0.8f);
					_cameraPanController.SlowMotion = true;
					SelectedCharacter.MoveToNode(gridNode);

					// Check if characters are on end positions
					if (SelectedCharacter == _her) {
						_sheIsAtTheEnd = gridNode._type == GridNode.GridNoteType.HerEnd;
					}
					else {
						_heIsAtTheEnd = gridNode._type == GridNode.GridNoteType.HisEnd;
					}

					// Level finished
					if (_sheIsAtTheEnd && _heIsAtTheEnd) {
						if (onlevelFinished != null) {
							onlevelFinished();
						}
					}
				}
			}
			// Deselection
//			else {
//
//				_her.Selected = false;
//				_him.Selected = false;
//			}
		}
	}
	
	private Character AnotherCharacter(Character character) {

		if (character == _her) {
			return _him;
		}
		else {
			return _her;
		}
	}

	public void StartGame() {
		
		_cameraPanController.enabled = true;
		_gestureManager.enabled = true;		
	}

	public void EndGame() {

		_cameraPanController.enabled = false;
		_gestureManager.enabled = false;		
	}
}
