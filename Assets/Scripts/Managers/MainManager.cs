﻿using UnityEngine;
using System.Collections;

public class MainManager : MonoBehaviour {

	public IntroManager _introManager;
	public GameplayManager _gameplayManager;
	public OutroManager _outroManager;
	public int levelNum = 0;

	void Start() {

		_introManager.onIntroFinished += OnIntroFinished;
		_gameplayManager.onlevelFinished += OnLevelFinished;
		_outroManager.onOutroFinished += OnOutroFinished;
	}

	public void OnIntroFinished() {

		_gameplayManager.StartGame();
	}

	public void OnLevelFinished() {

		LevelModel.Instance.UnlockLevel(levelNum + 1);

		SoundManager.Instance.PlayFinishedSound();
		SoundManager.Instance._destinationVolume = 0.0f;

		_gameplayManager.EndGame();
		_outroManager.enabled = true;
	}

	public void OnOutroFinished() {

		Application.LoadLevel(levelNum + 1);
	}
}
