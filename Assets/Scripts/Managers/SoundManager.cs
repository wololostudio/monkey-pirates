﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]

public class SoundManager : Singleton<SoundManager> {

	[HideInInspector]
	public float _destinationVolume = 1.0f;

	public AudioClip _moveSound;
	public AudioClip _doorOpenSound;
	public AudioClip _doorCloseSound;
	public AudioClip _finishedSound;
	public AudioClip _startSound;
	public AudioClip _selectedSound;

	private AudioSource _audioSource;
	
	void Start() {
		_audioSource = GetComponent<AudioSource>();
		_audioSource.volume = 0.0f;
	}

	void Update() {

		_audioSource.volume = Mathf.Lerp(_audioSource.volume, _destinationVolume, Time.deltaTime);
	}

	public void PlayMoveSound() {
		_audioSource.PlayOneShot(_moveSound);
	}

	public void PlayDoorOpenSound() {
		_audioSource.PlayOneShot(_doorOpenSound);
	}

	public void PlayDoorCloseSound() {
		_audioSource.PlayOneShot(_doorCloseSound);
	}

	public void PlayFinishedSound() {
		_audioSource.PlayOneShot(_finishedSound, 0.2f);
	}

	public void PlayStartSound() {
		_audioSource.PlayOneShot(_startSound);
	}

	public void PlaySelectedSound() {
		_audioSource.PlayOneShot(_selectedSound);
	}
}
