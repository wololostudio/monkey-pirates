﻿using UnityEngine;
using System.Collections;

public class CharacterSelection : MonoBehaviour {
	
	public SelectionSquare _selectionSquare;

	public bool Selected { 
		get {
			return _selected;
		}
		set {

			if (value == _selected) {
				return;
			}

			_selected = value;

			if (_selected) {
				_selectionSquare.StartPulsing();
			}
			else {
				_selectionSquare.StopPulsing();
			}
		}
	}

	private bool _selected;
}
