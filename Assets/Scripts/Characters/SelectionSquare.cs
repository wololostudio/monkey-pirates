﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(SpriteRenderer))]

public class SelectionSquare : MonoBehaviour {

	public float _minAlpha = 0.3f;
	public float _maxAlpha = 0.6f;
	public float _pulseDuration = 1.0f;

	private SpriteRenderer _spriteRenderer;
	private Color _startColor;
	private float _alpha;
	private bool _isPulsing;
	private float _elapsedTime;

	void Start () {

		_spriteRenderer = GetComponent<SpriteRenderer>();
		_startColor = _spriteRenderer.color;
		StartPulsing();
	}

	void Update () {
	
		if (_isPulsing) {

			if (_alpha >= _minAlpha) {
				_alpha = Mathf.Lerp(_minAlpha, _maxAlpha, Mathf.Sin(_elapsedTime / _pulseDuration) * 0.5f + 0.5f);
				_elapsedTime += Time.deltaTime;
			}
			else {
				_elapsedTime = 0.0f;
				_alpha += Time.deltaTime / _pulseDuration;
			}
		}
		else {
			_alpha -= Time.deltaTime / _pulseDuration;
		}

		if (_alpha < 0.0f) {
			_alpha = 0.0f;
			enabled = false;
		}

		_startColor.a = _alpha;
		_spriteRenderer.color = _startColor; 
	}

	public void StartPulsing() {

		_isPulsing = true;
		enabled = true;
	}

	public void StopPulsing() {

		_isPulsing = false;
	}
}
