﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

	public AnimationCurve _moveAnimationCurve;
	public float _moveAnimationDuration = 0.4f;
	public Color _cubeColor;
	public Material _cubeMaterial;
    public Quaternion lastRotation;
	
	public GridNode GridNode {
		get {
			return _gridNode;
		}
		set {
			if (_gridNode != null) {
				_gridNode.OccupiedByCharacter = false;
			}

			_gridNode = value;
			_gridNode.OccupiedByCharacter = true;
		}
	}

	public bool Selected {
		get {

			return _selected;
		}
		set {

			if (_selected == value) {
				return;
			}

			_selected = value;

			_animator.SetBool("Selected", _selected);
			ShowDestinationCircles(_selected);

			if (_selected) {
				SoundManager.Instance.PlaySelectedSound();
			}
		}
	}

	public bool CanBeSelected {
		get {
			return _canBeSelected;
		}
	}

	private bool _selected;
	private Transform _transform;
	private Animator _animator;
	
	private bool _canBeSelected;
	private float _disabledFactorDestination;
	private float _disabledFactor;

	private GridNode _gridNode;

	void Start() {

		_transform = transform;
		_animator = GetComponent<Animator>();
	}

	void Update() {

        _disabledFactor = Mathf.Lerp(_disabledFactor, _disabledFactorDestination, Time.deltaTime * 7.0f);
        _animator.SetFloat("DisabledFactor", _disabledFactor);
		_cubeMaterial.color = _cubeColor * (1.0f - _disabledFactor);
	}

	private void ShowDestinationCircles(bool show) {

		GridConnection[] gridConnections = _gridNode.AllConnections;
		
		for (int i = 0; i < gridConnections.Length; i++) {
			if (gridConnections[i] && gridConnections[i]._blocked == false) {
				GridNode anotherGridNode = gridConnections[i].AnotherGridNode(_gridNode);
				if (anotherGridNode.OccupiedByCharacter == false) {
					anotherGridNode.ShowDestinationCircle = show;
				}
			}
		}
	}

	public void MoveToNode(GridNode destinationGridNode) {

		ShowDestinationCircles(false);
		GridNode = destinationGridNode;
		StartCoroutine(MoveToNodeCoroutine(destinationGridNode));
	}

	private IEnumerator MoveToNodeCoroutine(GridNode destinationNode) {

		SoundManager.Instance.PlayMoveSound();

		float elapsedTime = 0.0f;

		Vector3 startPos = _transform.position;
        Quaternion startQuat = _transform.rotation;
        // Quaternion destinationQuat = Euler;
		Vector3 destinationPos = _gridNode.transform.position;
        Vector3 _direction;
        Vector3 _direction2 = new Vector3(20f, _transform.rotation.y,_transform.rotation.z);
        Quaternion _lookRotation;
        Quaternion angle = Quaternion.LookRotation(_direction2);

        _direction = (destinationPos - transform.position).normalized;
        while (elapsedTime < _moveAnimationDuration) {

			elapsedTime += Time.deltaTime;
			yield return new WaitForEndOfFrame();

			_transform.position = Vector3.Lerp(startPos, destinationPos, _moveAnimationCurve.Evaluate(elapsedTime / _moveAnimationDuration));
            _lookRotation = Quaternion.LookRotation(_direction);
            _transform.rotation = Quaternion.Slerp(transform.rotation,_lookRotation, Time.deltaTime*10.1f);
            // _transform.rotation = Quaternion.Slerp(transform.rotation,angle, Time.deltaTime*10.1f);
            _animator.SetBool("Moving",true);
        }
		ShowDestinationCircles(true);
        _animator.SetBool("Moving",false);
	}

	public void SightHasBeenInterupted() {

		if (!_selected) {
			_canBeSelected = false;
			_disabledFactorDestination = 1.0f;
		}
	}

	public void SightHasBeenConnected() {

		_canBeSelected = true;
		_disabledFactorDestination = 0.0f;
	}
}
